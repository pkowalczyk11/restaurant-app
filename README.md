# Restaurant app

Project created by [Barbara Gaweł-Kucab](https://gitlab.com/bgawkuc) and [Piotr Kowalczyk](https://gitlab.com/pkowalczyk11) for ZTP classes at Cracow University of Technlogy.

Swagger documentation can be found [here](http://localhost:8080/swagger-ui/index.html).

### Getting started

##### Without docker

###### Backend:
- go to `backend` folder
- in `backend/src/main/resources/application.properties` file replace `${SPRING_DATASOURCE_URL}` with `jdbc:h2:file:./restaurant-db`
- run `mvn spring-boot:run`

###### Frontend:
- go to `frontend` folder
- create `.env` file with the same variables as in the `.env.example` file but replace `REACT_APP_API_BASE_URL` with `http://localhost:8080/api`
- run `npm install & npm run start`

##### With docker

- in the root directory of the repository, in the `docker-compose.yml` file set `NODE_ENV` variable to `development` and set `REACT_APP_API_BASE_URL` to `http://localhost:8080/api`

### Tests

##### Backend

Backend tests are created using JUnit 5 library and AssertJ for assertions. To run then run `mvn test` in the `backend` folder. It will fire unit tests for business logic of the application.

##### Frontend

Frontend tests are created using the jest framework. To fire them, go into the `frontend` folder and run `npm run test` command. This will run the component and screen tests.

### About the app

The app was created to allow online ordering at restaurant.

Users have the ability to:

- create an account
- log in
- browse the list of dishes and filter it
- add dishes to the cart
- order a basket

In addition, the user who is an administrator has the ability to add new dishes to the list.

### Techonologies used

##### Backend

- created using [spring-boot](https://github.com/spring-projects/spring-boot) framework
- main external libraries used:
  - [JSON Web Token](https://jwt.io/)
  - FasterXML Jackson

##### Frontend

- created using [react](https://github.com/facebook/react) library
- main external libraries used:
  - [axios](https://github.com/axios/axios)
  - [formik](https://github.com/jaredpalmer/formik)
  - [styled-components](https://github.com/styled-components/styled-components)
  - [material-ui](https://github.com/mui/material-ui)

##### Database

The app uses the [h2](https://github.com/h2database/h2database) database.

Database schema:

<img src="images/database-schema.png" alt="database-schema" width="500"/>

### Pipeline

##### Backend

<img src="images/pipeline-backend.png" alt="backend" width="800"/>

##### Frontend

<img src="images/pipeline-frontend.png" alt="frontend" width="800"/>

### App screens

##### Sign up

<img src="images/sign-up.png" alt="sign-up" width="800"/>

##### Sign in

<img src="images/sign-in.png" alt="sign-in" width="800"/>

##### Dish list

User can browse the list and select a dish, its quantity and add it to the cart
<img src="images/dish-list.png" alt="dish-list" width="800"/>

User can use search bar and price filter to find the right dishes. In addition, thanks to the pagination, up to 3 dishes are displayed on the page at once
<img src="images/dish-list-filtered.png" alt="dish-list-filtered" width="800"/>

##### Cart

<img src="images/cart-empty.png" alt="cart-empty" width="800"/>

Dishes added to the cart are displayed on this screen. User can change their quantity and make an order
<img src="images/cart.png" alt="cart" width="800"/>

##### Add dish screen

Screen available only to administrators. They can add a new dish to the list. The form includes name, description and price
<img src="images/add-dish.png" alt="add-dish" width="800"/>

##### Form validation

All forms in the app have validations. In case of entering an incorrect type or missing data, appropriate defect messages are displayed
<img src="images/form-validation.png" alt="form-validation" width="800"/>

##### Toast notifications

When the action is executed, the app displays a message whether the action passed or failed. It's displayed using toast notification
<img src="images/toast-success.png" alt="toast-success" width="800"/>
<img src="images/toast-error.png" alt="toast-error" width="800"/>

### UML

Use case diagram for new dish adding operation done by administrator.
<img src="images/restaurant_use_case.png" alt="toast-error" width="800"/>
