export const LOGIN_URL = window.__RUNTIME_CONFIG__.REACT_APP_API_BASE_URL + '/auth/login';
export const SIGNUP_URL = window.__RUNTIME_CONFIG__.REACT_APP_API_BASE_URL + '/auth/signup';

export const USER_URL = window.__RUNTIME_CONFIG__.REACT_APP_API_BASE_URL + '/users';

export const DISHES_URL = window.__RUNTIME_CONFIG__.REACT_APP_API_BASE_URL + '/dishes';

export const BASE_CART_URL = window.__RUNTIME_CONFIG__.REACT_APP_API_BASE_URL + '/carts/user';
export const PENDING_CART_URL = (userId: number) => BASE_CART_URL + `/${userId}/pending`;
export const UPDATE_CART_URL = (userId: number) => BASE_CART_URL + `/${userId}/updateQuantity`;
export const ORDER_CART_URL = (userId: number) =>
  window.__RUNTIME_CONFIG__.REACT_APP_API_BASE_URL + `/carts/${userId}/orderCart`;
