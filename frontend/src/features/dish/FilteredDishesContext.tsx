import { FormikProps, useFormik } from 'formik';
import React, { ReactNode, useContext, useEffect, useState } from 'react';
import { Dish } from 'types';
import { useDishContext } from './DishContext';
import { MAX_PRICE, MIN_PRICE } from 'utils/constants';

type Values = {
  searchValue: string;
  minPrice: number;
  maxPrice: number;
};

type FilteredDishesContextState = {
  filteredDishes: Dish[];
  pageDishes: Dish[];
  formik: FormikProps<Values>;
  currentPage: number;
  handlePageChange: (page: number) => void;
  handleChangePriceRange: (value: number[]) => void;
};

const INITIAL_STATE = {
  filteredDishes: [],
  pageDishes: [],
  formik: {} as FormikProps<Values>,
  currentPage: 1,
  handlePageChange: () => {},
  handleChangePriceRange: () => {},
};

export const DISHES_PER_PAGE = 3;

export const FilteredDishesContext = React.createContext<FilteredDishesContextState>(INITIAL_STATE);

export function useFilteredDishesContext(): FilteredDishesContextState {
  return useContext<FilteredDishesContextState>(FilteredDishesContext);
}

export function FilteredDishesProvider({ children }: { children: ReactNode }) {
  const { dishes } = useDishContext();

  const [filteredDishes, setFilteredDishes] = useState<Dish[]>([]);
  const [currentPage, setCurrentPage] = useState<number>(1);

  const lastDishIdx = currentPage * DISHES_PER_PAGE;
  const firstDishIdx = lastDishIdx - DISHES_PER_PAGE;

  const pageDishes = filteredDishes.slice(firstDishIdx, lastDishIdx);

  const formik = useFormik<Values>({
    initialValues: {
      searchValue: '',
      minPrice: MIN_PRICE,
      maxPrice: MAX_PRICE,
    },
    onSubmit: () => {},
  });

  const handleChangePriceRange = (value: number[]) => {
    formik.setFieldValue('minPrice', value[0]);
    formik.setFieldValue('maxPrice', value[1]);
  };

  const handlePageChange = (page: number) => {
    setCurrentPage(page);
  };

  const checkPrice = (dish: Dish) => {
    return dish.price >= formik.values.minPrice && dish.price <= formik.values.maxPrice;
  };

  useEffect(() => {
    const filtered = formik.values.searchValue
      ? dishes.filter(
          (dish) => dish.name.toLowerCase().includes(formik.values.searchValue) && checkPrice(dish)
        )
      : dishes.filter((dish) => checkPrice(dish));

    setFilteredDishes(filtered);
  }, [formik.values, dishes]);

  return (
    <FilteredDishesContext.Provider
      value={{
        filteredDishes,
        pageDishes,
        formik,
        currentPage,
        handlePageChange,
        handleChangePriceRange,
      }}>
      {children}
    </FilteredDishesContext.Provider>
  );
}
