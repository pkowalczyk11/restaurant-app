import { DISHES_URL } from 'config/URLS';
import { jwtInterceptor } from 'features/auth/jwtInterceptor';
import React, { ReactNode, useContext, useEffect, useState } from 'react';
import { AddDishRequestType, Dish } from 'types';

type DishContextState = {
  dishes: Dish[];
  addDish: (_addDish: AddDishRequestType) => Promise<void>;
  getDishes: () => Promise<void>;
};

const INITIAL_STATE = {
  dishes: [],
  addDish: async (): Promise<void> => {},
  getDishes: async (): Promise<void> => {},
};

export const DishContext = React.createContext<DishContextState>(INITIAL_STATE);

export function useDishContext(): DishContextState {
  return useContext<DishContextState>(DishContext);
}

export function DishProvider({ children }: { children: ReactNode }) {
  const [dishes, setDishes] = useState<Dish[]>([]);

  const getDishes = async () => {
    try {
      const apiResponse = await jwtInterceptor.get(DISHES_URL);
      setDishes(apiResponse.data);
    } catch (e) {
      console.log('getDishes error', e);
    }
  };

  const addDish = async (dish: AddDishRequestType) => {
    try {
      await jwtInterceptor.post(DISHES_URL, dish);
    } catch (e) {
      console.log('addDish error', e);
      throw e;
    }
  };

  useEffect(() => {
    getDishes();
  }, []);

  return (
    <DishContext.Provider value={{ dishes, addDish, getDishes }}>{children}</DishContext.Provider>
  );
}
