import { ORDER_CART_URL, PENDING_CART_URL, UPDATE_CART_URL } from 'config/URLS';
import { jwtInterceptor, useAuthContext } from 'features/auth';
import React, { ReactNode, useContext, useEffect, useState } from 'react';
import { CartDish, RequestUpdateCartDish, ResponseCartDish } from 'types';

type CartContextState = {
  cartDishes: CartDish[];
  updateCart: (updateCartDish: RequestUpdateCartDish) => Promise<void>;
  orderCart: () => Promise<void>;
  getDishQuantity: (dishId: number) => number;
};

const INITIAL_STATE = {
  cartDishes: [],
  updateCart: async (): Promise<void> => {},
  orderCart: async (): Promise<void> => {},
  getDishQuantity: () => 0,
};

export const CartContext = React.createContext<CartContextState>(INITIAL_STATE);

export function useCartContext(): CartContextState {
  return useContext<CartContextState>(CartContext);
}

export function CartProvider({ children }: { children: ReactNode }) {
  const [cartDishes, setCartDishes] = useState<CartDish[]>([]);

  const { authUser } = useAuthContext();

  const getPendingCart = async () => {
    if (authUser) {
      try {
        const apiResponse = await jwtInterceptor.get(PENDING_CART_URL(authUser.userId));
        const mappedDishes = apiResponse.data.dishes.map((dish: ResponseCartDish) => ({
          id: dish.id,
          name: dish.dishName,
          price: dish.price,
          quantity: dish.quantity,
        }));
        setCartDishes(mappedDishes);
      } catch (e) {
        // TODO: empty cart shouldn't be handled as error - backend fix needed
        const error = e as { response?: { data?: { message?: string } } };
        if (error?.response?.data?.message === 'No pending cart for given user') {
          setCartDishes([]);
          console.log('No pending cart for given user');
        } else {
          console.log('getPendingCart error', e);
        }
      }
    }
  };

  const updateCart = async (updateCartDish: RequestUpdateCartDish) => {
    if (authUser) {
      try {
        await jwtInterceptor.post(UPDATE_CART_URL(authUser.userId), updateCartDish);
        await getPendingCart();
      } catch (e) {
        console.log('updateCart error', e);
        throw e;
      }
    }
  };

  const orderCart = async () => {
    if (authUser) {
      try {
        const apiResponse = await jwtInterceptor.put(ORDER_CART_URL(authUser.userId));
        console.log(apiResponse);
        setCartDishes([]);
      } catch (e) {
        console.log('orderCart error', e);
        throw e;
      }
    }
  };

  const getDishQuantity = (dishId: number) => {
    return cartDishes.find((d) => d.id === dishId)?.quantity ?? 0;
  };

  useEffect(() => {
    if (authUser) {
      getPendingCart();
    } else {
      setCartDishes([]);
    }
  }, [authUser]);

  return (
    <CartContext.Provider
      value={{
        cartDishes,
        updateCart,
        orderCart,
        getDishQuantity,
      }}>
      {children}
    </CartContext.Provider>
  );
}
