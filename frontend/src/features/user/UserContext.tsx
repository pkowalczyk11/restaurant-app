import { USER_URL } from 'config/URLS';
import { useAuthContext } from 'features/auth';
import { jwtInterceptor } from 'features/auth/jwtInterceptor';
import React, { ReactNode, useContext, useEffect, useState } from 'react';
import { User } from 'types';

type UserContextState = {
  user: User | null;
};

const INITIAL_STATE = {
  user: null,
};

export const UserContext = React.createContext<UserContextState>(INITIAL_STATE);

export function useUserContext(): UserContextState {
  return useContext<UserContextState>(UserContext);
}

export function UserProvider({ children }: { children: ReactNode }) {
  const [user, setUser] = useState<User | null>(null);

  const { authUser } = useAuthContext();

  const getUser = async () => {
    if (authUser) {
      try {
        const apiResponse = await jwtInterceptor.get(USER_URL + `/${authUser.userId}`);
        const userData = apiResponse.data;
        setUser({
          name: userData.name,
          surname: userData.surname,
          isAdmin: userData.isAdmin,
          ...authUser,
        });
      } catch (e) {
        console.log('getUser error', e);
      }
    } else {
      setUser(null);
    }
  };

  useEffect(() => {
    getUser();
  }, [authUser]);

  return <UserContext.Provider value={{ user }}>{children}</UserContext.Provider>;
}
