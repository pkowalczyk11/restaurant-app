import axios from 'axios';
import { LOGIN_URL, SIGNUP_URL } from 'config/URLS';
import React, { ReactNode, useContext, useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { LogInUser, SignUpUser, AuthUser } from 'types';

type AuthContextState = {
  authUser: AuthUser | null;
  setAuthUser: React.Dispatch<React.SetStateAction<AuthUser | null>>;
  signUp: (_signUpUser: SignUpUser) => Promise<void>;
  logIn: (_logInUser: LogInUser) => Promise<void>;
  logOut: () => void;
};

const INITIAL_STATE = {
  authUser: null,
  setAuthUser: () => {},
  signUp: async (): Promise<void> => {},
  logIn: async (): Promise<void> => {},
  logOut: () => {},
};

export const AuthContext = React.createContext<AuthContextState>(INITIAL_STATE);

export function useAuthContext(): AuthContextState {
  return useContext<AuthContextState>(AuthContext);
}

export function AuthProvider({ children }: { children: ReactNode }) {
  const [authUser, setAuthUser] = useState<AuthUser | null>(null);

  const navigate = useNavigate();

  const signUp = async (signUpUser: SignUpUser) => {
    try {
      const apiResponse = await axios.post(SIGNUP_URL, signUpUser);
      navigate('/login');
      console.log(apiResponse.data);
    } catch (e) {
      console.log('signup error', e);
      throw e;
    }
  };

  const logIn = async (logInUser: LogInUser) => {
    try {
      const apiResponse = await axios.post(LOGIN_URL, logInUser);
      const userData = apiResponse.data;
      const user: AuthUser = {
        email: logInUser.email,
        userId: userData.id,
        authToken: userData.authToken,
      };
      setAuthUser(user);
      localStorage.setItem('authUser', JSON.stringify(user));
      console.log(userData, 'login');
    } catch (e) {
      console.log('login error', e);
      throw e;
    }
  };

  const logOut = () => {
    setAuthUser(null);
    localStorage.removeItem('authUser');
  };

  useEffect(() => {
    const storageUser = localStorage.getItem('authUser');
    if (storageUser) {
      setAuthUser(JSON.parse(storageUser));
    }
  }, []);

  return (
    <AuthContext.Provider value={{ authUser, setAuthUser, signUp, logIn, logOut }}>
      {children}
    </AuthContext.Provider>
  );
}
