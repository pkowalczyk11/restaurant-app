import axios from 'axios';
import { User } from '../../types';

export const jwtInterceptor = axios.create({
  headers: {
    'Content-Type': 'application/json',
  },
});

jwtInterceptor.interceptors.request.use(
  (config) => {
    const authUser = localStorage.getItem('authUser');
    if (authUser) {
      const parsedAuthData: User = JSON.parse(authUser);
      config.headers.Authorization = `Bearer ${parsedAuthData.authToken}`;
    }
    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);

jwtInterceptor.interceptors.response.use(
  (response) => {
    return response;
  },
  (error) => {
    return Promise.reject(error);
  }
);
