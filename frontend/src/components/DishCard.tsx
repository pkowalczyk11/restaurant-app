import { colors, Icon } from 'assets';
import { Button, Text, Toast, ToastMethods } from 'components';
import { useAuthContext } from 'features/auth';
import { useCartContext } from 'features/cart';
import { useRef, useState } from 'react';
import styled from 'styled-components';
import { TOAST_ERROR } from 'utils/constants';

type DishCardType = {
  id: number;
  name: string;
  price: number;
  description?: string;
  quantity?: number;
};

export function DishCard({ dish }: { dish: DishCardType }) {
  const { authUser } = useAuthContext();
  const { updateCart, getDishQuantity } = useCartContext();

  const dishQuantity = authUser ? getDishQuantity(dish.id) : 0;

  const [quantity, setQuantity] = useState<number>(dish.quantity ?? dishQuantity);

  const updateCartSuccessToastRef = useRef<ToastMethods>(null);
  const updateCartErrorToastRef = useRef<ToastMethods>(null);
  const quantityErrorToastRef = useRef<ToastMethods>(null);

  const isCart = !!dish.quantity;
  const isMinQuantity = quantity === 0;
  const isMaxQuantity = quantity === 9;

  const handleChangeQuantity = (type: 'plus' | 'minus') => {
    if ((isMinQuantity && type === 'minus') || (isMaxQuantity && type === 'plus')) return;
    if (type === 'plus') {
      setQuantity((prev) => prev + 1);
    } else {
      setQuantity((prev) => prev - 1);
    }
  };

  const handleUpdateCart = async () => {
    if (!quantity) {
      quantityErrorToastRef.current?.showToast();
      return;
    }
    try {
      await updateCart({ dishId: dish.id, quantity });
      updateCartSuccessToastRef.current?.showToast();
    } catch (e) {
      updateCartErrorToastRef.current?.showToast();
    }
  };

  return (
    <CardContainer>
      <Toast ref={quantityErrorToastRef} message="Minimum quantity is 1" variant="error" />
      <Toast ref={updateCartErrorToastRef} message={TOAST_ERROR} variant="error" />
      <Toast ref={updateCartSuccessToastRef} message="Cart succesfully updated" />
      <div>
        <Text.Subtitle color={colors.blueberry.blueberry550}>{dish.name}</Text.Subtitle>
        {dish.description && (
          <CardLine>
            <Icon.Book color={colors.gray.gray600} width={20} height={20} />
            <Text.Subtitle type={3} color={colors.gray.gray600}>
              {dish.description}
            </Text.Subtitle>
          </CardLine>
        )}
        <CardLine>
          <Icon.Dollar color={colors.gray.gray600} width={20} height={20} />
          <Text.Subtitle type={3} color={colors.gray.gray600}>
            {dish.price}
          </Text.Subtitle>
        </CardLine>
      </div>
      {authUser && (
        <Right>
          <QuantityContainer>
            <QuantityButton disabled={isMinQuantity} onClick={() => handleChangeQuantity('minus')}>
              -
            </QuantityButton>
            <Text.Subtitle color={colors.gray.gray600}>{quantity}</Text.Subtitle>
            <QuantityButton disabled={isMaxQuantity} onClick={() => handleChangeQuantity('plus')}>
              +
            </QuantityButton>
          </QuantityContainer>
          <Button
            text={isCart ? 'Change quantity' : 'Add to cart'}
            size="small"
            onClick={handleUpdateCart}
          />
        </Right>
      )}
    </CardContainer>
  );
}

const CardContainer = styled.div`
  border: 1px solid ${colors.gray.gray600};
  padding: 16px;
  border-radius: 4px;
  margin-bottom: 24px;
  width: 60%;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`;

const CardLine = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  column-gap: 12px;
  margin-top: 4px;
`;

const Right = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
`;

const QuantityContainer = styled.div`
  display: flex;
  column-gap: 8px;
  justify-content: center;
`;

const QuantityButton = styled(Text.Subtitle)<{ disabled: boolean }>`
  color: ${(p) => (p.disabled ? colors.gray.gray250 : colors.gray.gray600)};

  &:hover {
    cursor: pointer;
  }
`;
