import { Snackbar } from '@mui/material';
import { colors, Icon } from 'assets';
import React, { forwardRef, useImperativeHandle, useState } from 'react';
import { Text } from './Text';
import styled from 'styled-components';

export type ToastMethods = {
  showToast: () => void;
  hideToast: () => void;
};

type Variant = 'success' | 'error';

interface ToastProps {
  message: string;
  variant?: Variant;
}

export const Toast = forwardRef<ToastMethods, ToastProps>(
  ({ message, variant = 'success' }, ref) => {
    const [open, setOpen] = useState<boolean>(false);

    const handleClose = (_event: React.SyntheticEvent | Event, reason?: string) => {
      if (reason === 'clickaway') {
        return;
      }
      setOpen(false);
    };

    useImperativeHandle(ref, () => ({
      showToast: () => {
        setOpen(true);
      },
      hideToast: () => {
        setOpen(false);
      },
    }));

    return (
      <Snackbar
        open={open}
        autoHideDuration={3000}
        anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
        onClose={handleClose}>
        <ContentContainer variant={variant}>
          <Text.Body type={1} color={colors.base.cream}>
            {message}
          </Text.Body>
          <StyledButton onClick={handleClose} variant={variant}>
            <Icon.Close width={30} height={30} color={colors.base.cream} />
          </StyledButton>
        </ContentContainer>
      </Snackbar>
    );
  }
);

const ContentContainer = styled.div<{ variant: Variant }>`
  background-color: ${(p) =>
    p.variant === 'success' ? colors.green.green450 : colors.accent.pink};
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 12px 24px;
  width: 300px;
  border-radius: 4px;
`;

const StyledButton = styled.button<{ variant: Variant }>`
  background-color: ${(p) =>
    p.variant === 'success' ? colors.green.green450 : colors.accent.pink};
  border: none;
  display: flex;
  justify-content: center;
  align-items: center;

  &:hover {
    cursor: pointer;
  }
`;
