import { colors } from 'assets';
import styled, { css, CSSProperties } from 'styled-components';

type Props = {
  color?: string;
  style?: CSSProperties;
  type?: 1 | 2 | 3;
};

const BaseText = styled.p.attrs((p) => ({
  style: p.style,
}))<Props>`
  color: ${(p) => p.color || colors.gray.gray900};
  font-family: 'Manrope';
  font-weight: 500;

  ${(p) => {
    return {
      1: css`
        font-size: 18px;
        line-height: 26px;
      `,
      2: css`
        font-size: 16px;
        line-height: 24px;
      `,
      3: css`
        font-size: 14px;
        line-height: 22px;
      `,
    }[p.type || 2];
  }};
`;

const Title = styled(BaseText)<Props>`
  font-weight: 700;

  ${(p) => {
    return {
      1: css`
        font-size: 48px;
        line-height: 56px;
      `,
      2: css`
        font-size: 36px;
        line-height: 44px;
      `,
      3: css`
        font-size: 28px;
        line-height: 36px;
        font-weight: 500;
      `,
    }[p.type || 1];
  }};
`;

const Subtitle = styled(BaseText)<Props>`
  ${(p) => {
    return {
      1: css`
        font-size: 28px;
        line-height: 36px;
      `,
      2: css`
        font-size: 24px;
        line-height: 32px;
      `,
      3: css`
        font-size: 20px;
        line-height: 28px;
      `,
    }[p.type || 1];
  }}
`;

export const Text = {
  Title,
  Subtitle,
  Body: BaseText,
};
