import { colors } from 'assets';
import styled from 'styled-components';
import { Text } from './Text';

type Size = 'small' | 'big';

type Props = {
  text: string;
  size?: Size;
  onClick?: React.MouseEventHandler<HTMLButtonElement>;
};
export function Button({ text, size = 'big', onClick }: Props) {
  return (
    <StyledButton size={size} onClick={onClick}>
      <Text.Body type={1} color={colors.base.cream}>
        {text}
      </Text.Body>
    </StyledButton>
  );
}

const StyledButton = styled.button<{ size: Size }>`
  background-color: ${colors.blueberry.blueberry550};
  border: none;
  color: ${colors.base.cream};
  width: ${(p) => (p.size === 'big' ? 500 : 180)}px;
  border-radius: 32px;
  padding: ${(p) => (p.size === 'big' ? 10 : 6)}px 0;

  &:hover {
    background-color: ${colors.blueberry.blueberry450};
    cursor: pointer;
  }
`;
