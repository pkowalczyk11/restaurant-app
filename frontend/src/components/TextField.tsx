import { TextField as MuiTextField, TextFieldProps as MuiTextFieldProps } from '@mui/material';
import styled from 'styled-components';
import { Text } from './Text';
import { colors, Icon } from 'assets';

type Props = {
  errorText?: string;
} & MuiTextFieldProps;

export function TextField({ error, errorText, ...props }: Props) {
  const displayError = !!(error && errorText);

  return (
    <Container>
      <TextFieldContainer $addMargin={!displayError}>
        <StyledTextField error={error} {...props} />
      </TextFieldContainer>
      {displayError && (
        <ErrorContainer>
          <Icon.ExclamationCircle color={colors.accent.cherry} />
          <Text.Body color={colors.accent.cherry} type={3}>
            {errorText}
          </Text.Body>
        </ErrorContainer>
      )}
    </Container>
  );
}

const Container = styled.div`
  margin-bottom: 8px;
`;

const TextFieldContainer = styled.div<{ $addMargin: boolean }>`
  margin-bottom: ${(p) => p.$addMargin && '24px'};
`;

const StyledTextField = styled(MuiTextField)`
  & label {
    font-family: 'Manrope';
  }

  &.MuiOutlinedInput-notchedOutline {
    font-size: 28px;
  }

  & .MuiOutlinedInput-root {
    & fieldset,
    &:hover fieldset {
      border-width: 2px;
      font-family: 'Manrope';
      /* border-color: 'pink'; */
    }

    color: 'orange';
    width: 500px;

    &.Mui-focused {
      & fieldset {
        /* border-color: 'red'; */
      }
    }
  }
`;

const ErrorContainer = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  column-gap: 4px;
  margin-top: 2px;
`;
