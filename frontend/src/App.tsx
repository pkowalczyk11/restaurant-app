import { AuthProvider } from 'features/auth';
import { CartProvider } from 'features/cart';
import { FilteredDishesProvider, DishProvider } from 'features/dish';
import { UserProvider } from 'features/user';
import { NavigationProvider } from 'navigation';

const App = () => {
  return (
    <div>
      <AuthProvider>
        <UserProvider>
          <DishProvider>
            <FilteredDishesProvider>
              <CartProvider>
                <NavigationProvider />
              </CartProvider>
            </FilteredDishesProvider>
          </DishProvider>
        </UserProvider>
      </AuthProvider>
    </div>
  );
};

export default App;
