import { Route, Routes } from 'react-router-dom';
import { AddDish, DishList } from 'screens/dish';
import { LogIn, SignUp } from 'screens/auth';
import { NavBar } from 'screens/navigation';
import { ProtectedRoute } from './ProtectedRoute';
import { Cart } from 'screens/cart';

export const NavigationProvider = () => (
  <>
    <NavBar />
    <Routes>
      <Route path="/" element={<DishList />} />
      <Route
        path="/login"
        element={
          <ProtectedRoute accessBy="non-authenticated">
            <LogIn />
          </ProtectedRoute>
        }
      />
      <Route
        path="/signup"
        element={
          <ProtectedRoute accessBy="non-authenticated">
            <SignUp />
          </ProtectedRoute>
        }
      />
      <Route
        path="/addDish"
        element={
          <ProtectedRoute accessBy="admin">
            <AddDish />
          </ProtectedRoute>
        }
      />
      <Route
        path="/cart"
        element={
          <ProtectedRoute accessBy="authenticated">
            <Cart />
          </ProtectedRoute>
        }
      />
    </Routes>
  </>
);
