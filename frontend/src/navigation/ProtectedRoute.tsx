import { useAuthContext } from 'features/auth';
import { useUserContext } from 'features/user';
import React from 'react';
import { Navigate } from 'react-router-dom';

interface ProtectedRouteProps {
  children: React.ReactNode;
  accessBy: 'non-authenticated' | 'authenticated' | 'admin';
}

export const ProtectedRoute = ({ children, accessBy }: ProtectedRouteProps) => {
  const { authUser } = useAuthContext();
  const { user } = useUserContext();

  if (accessBy === 'non-authenticated' && !authUser) {
    return <>{children}</>;
  } else if (accessBy === 'authenticated' && authUser) {
    return <>{children}</>;
  } else if (accessBy === 'admin' && user && user.isAdmin) {
    return <>{children}</>;
  }

  return <Navigate to="/"></Navigate>;
};
