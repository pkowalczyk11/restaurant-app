export const colors = {
  base: {
    black: '#000000',
    white: '#FFFFFF',
    cream: '#FAFDF0',
  },
  accent: {
    apple: '#05EC00',
    cherry: '#FF2B5E',
    pink: '#E33AFF',
  },
  blueberry: {
    blueberry750: '#050442',
    blueberry650: '#170059',
    blueberry550: '#1E0072',
    blueberry450: '#25008C',
    blueberry250: '#9F93C1',
  },
  gray: {
    gray900: '#1A1B18',
    gray800: '#353630',
    gray600: '#6A6C60',
    gray400: '#9D9F93',
    gray250: '#C2C3BB',
  },
  green: {
    green550: '#4EA044',
    green450: '#3AD028',
  },
};
