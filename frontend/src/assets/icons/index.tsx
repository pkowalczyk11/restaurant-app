import { ReactComponent as Cutlery } from './cutlery.svg';
import { ReactComponent as Cart } from './cart.svg';
import { ReactComponent as Person } from './person.svg';
import { ReactComponent as PersonAdd } from './person-add.svg';
import { ReactComponent as ExclamationCircle } from './exclamation-cricle.svg';
import { ReactComponent as Book } from './book.svg';
import { ReactComponent as Dollar } from './dollar.svg';
import { ReactComponent as PlusCircle } from './plus-circle.svg';
import { ReactComponent as Close } from './close.svg';

export const Icon = {
  Cutlery,
  Cart,
  Person,
  PersonAdd,
  ExclamationCircle,
  Book,
  Dollar,
  PlusCircle,
  Close,
};
