export interface LogInUser {
  email: string;
  password: string;
}

export interface SignUpUser extends LogInUser {
  name: string;
  surname: string;
  isAdmin?: boolean;
}

export interface AuthUser {
  email: string;
  userId: number;
  authToken: string;
}

export interface User extends AuthUser {
  name: string;
  surname: string;
  isAdmin: boolean;
}
