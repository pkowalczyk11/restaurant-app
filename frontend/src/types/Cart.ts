export interface CartDish {
  name: string;
  price: number;
  quantity: number;
  id: number;
}

export interface ResponseCartDish {
  dishName: string;
  price: number;
  quantity: number;
  id: number;
}

export interface RequestUpdateCartDish {
  dishId: number;
  quantity: number;
}
