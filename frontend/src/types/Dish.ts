export interface Dish {
  id: number;
  name: string;
  description: string;
  price: number;
}

export interface AddDishRequestType {
  userId: number;
  name: string;
  description: string;
  price: number;
}
