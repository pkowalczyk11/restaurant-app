export const MIN_PRICE = 1;
export const MAX_PRICE = 100;

export const TOAST_ERROR = 'Something went wrong. Please try again';
