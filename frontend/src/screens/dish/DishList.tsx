import { colors } from 'assets';
import { Container, DishCard, Text, TextField } from 'components';
import { DISHES_PER_PAGE, useFilteredDishesContext } from 'features/dish';
import { Title } from 'screens/common';
import styled from 'styled-components';
import { Pagination, PaginationItem, Slider } from '@mui/material';
import { MAX_PRICE, MIN_PRICE } from 'utils/constants';

export function DishList() {
  const {
    filteredDishes,
    pageDishes,
    formik,
    currentPage,
    handlePageChange,
    handleChangePriceRange,
  } = useFilteredDishesContext();

  return (
    <Container>
      <FiltersContainer>
        <Filter style={{ marginLeft: -12 }}>
          <Text.Subtitle type={2}>Search bar</Text.Subtitle>
          <TextField
            label="Search"
            name="searchValue"
            type="string"
            placeholder="Enter dish name"
            value={formik.values.searchValue}
            onChange={formik.handleChange('searchValue')}
          />
        </Filter>
        <Filter>
          <Text.Subtitle type={2}>Price filter</Text.Subtitle>
          <SliderContainer>
            <Slider
              value={[formik.values.minPrice, formik.values.maxPrice]}
              onChange={(_event, value) =>
                handleChangePriceRange(typeof value === 'number' ? [MIN_PRICE, MAX_PRICE] : value)
              }
              valueLabelDisplay="auto"
              style={{ color: colors.blueberry.blueberry550 }}
              min={MIN_PRICE}
              max={MAX_PRICE}
            />
          </SliderContainer>
        </Filter>
      </FiltersContainer>
      <Title type={3}>Dish list</Title>
      <div>
        {pageDishes.map((d) => (
          <DishCard key={d.id} dish={d} />
        ))}
      </div>
      <Pagination
        count={Math.ceil(filteredDishes.length / DISHES_PER_PAGE)}
        page={currentPage}
        onChange={(_e, page) => handlePageChange(page)}
        renderItem={(item) => (
          <PaginationItem
            {...item}
            component="button"
            onClick={() => handlePageChange(item.page ?? 1)}
          />
        )}
      />
    </Container>
  );
}

const FiltersContainer = styled.div`
  display: flex;
  flex-wrap: wrap;
  column-gap: 32px;
  width: 60%;
  justify-content: space-between;
  margin: 12px;
`;

const Filter = styled.div`
  display: flex;
  flex-direction: column;
  row-gap: 12px;
`;

const SliderContainer = styled.div`
  width: 400px;
  display: flex;
  align-items: center;
  height: 56px;
`;
