import { Button, Container, TextField, Toast, ToastMethods } from 'components';
import { useDishContext } from 'features/dish';
import { useUserContext } from 'features/user';
import { useFormik } from 'formik';
import { useRef } from 'react';
import { useNavigate } from 'react-router-dom';
import { FormContainer, Title } from 'screens/common';
import { MAX_PRICE, MIN_PRICE, TOAST_ERROR } from 'utils/constants';
import * as yup from 'yup';

type Values = {
  name: string;
  description: string;
  price: number | undefined;
};

const initialValues: Values = {
  name: '',
  description: '',
  price: undefined,
};

export function AddDish() {
  const { addDish, getDishes } = useDishContext();
  const { user } = useUserContext();

  const navigate = useNavigate();

  const addDishSuccessToastRef = useRef<ToastMethods>(null);
  const addDishErrorToastRef = useRef<ToastMethods>(null);

  const validationSchema = yup.object().shape({
    name: yup.string().required('Name is required'),
    description: yup.string().required('Description is required'),
    price: yup
      .number()
      .required('Price is required')
      .min(MIN_PRICE, `Minimum price is ${MIN_PRICE}$`)
      .max(MAX_PRICE, `Minimum price is ${MAX_PRICE}$`),
  });

  const { values, errors, touched, handleChange, submitForm, resetForm, setFieldValue } =
    useFormik<Values>({
      initialValues,
      validationSchema,
      onSubmit: async () => {
        if (user && user.isAdmin) {
          const price = values.price ?? 0;
          const dish = { ...values, userId: user.userId, price };
          try {
            await addDish(dish);
            addDishSuccessToastRef.current?.showToast();
            resetForm();
            setFieldValue('price', undefined);
            setTimeout(() => {
              navigate('/');
            }, 1500);
          } catch (e) {
            addDishErrorToastRef.current?.showToast();
          }
          await getDishes();
        }
      },
    });

  return (
    <Container>
      <FormContainer>
        <Toast ref={addDishErrorToastRef} message={TOAST_ERROR} variant="error" />
        <Toast ref={addDishSuccessToastRef} message="Dish added correctly" />
        <Title type={3}>Add dish form</Title>
        <TextField
          label="Name"
          name="name"
          type="string"
          value={values.name}
          error={touched.name && !!errors.name}
          errorText={errors.name}
          onChange={handleChange('name')}
        />
        <TextField
          label="Description"
          name="description"
          type="string"
          value={values.description}
          error={touched.description && !!errors.description}
          errorText={errors.description}
          onChange={handleChange('description')}
        />
        <TextField
          label="Price"
          name="price"
          type="number"
          value={values.price}
          error={touched.price && !!errors.price}
          errorText={errors.price}
          onChange={handleChange('price')}
        />
      </FormContainer>
      <Button text="Add dish" onClick={submitForm} />
    </Container>
  );
}
