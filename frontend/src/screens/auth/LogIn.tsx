import { Button, Container, TextField, Toast, ToastMethods } from 'components';
import { useFormik } from 'formik';
import * as yup from 'yup';
import { FormContainer, Title } from 'screens/common';
import { useAuthContext } from 'features/auth';
import { useRef } from 'react';
import { TOAST_ERROR } from 'utils/constants';

type Values = {
  email: string;
  password: string;
};

const initialValues: Values = {
  email: '',
  password: '',
};

export function LogIn() {
  const { logIn } = useAuthContext();

  const loginSuccessToastRef = useRef<ToastMethods>(null);
  const loginErrorToastRef = useRef<ToastMethods>(null);

  const validationSchema = yup.object().shape({
    email: yup.string().required('E-mail is required').email('E-mail must be valid'),
    password: yup.string().required('Password is required'),
  });

  const { values, errors, touched, handleChange, submitForm } = useFormik<Values>({
    initialValues,
    validationSchema,
    onSubmit: async () => {
      try {
        await logIn(values);
        loginSuccessToastRef.current?.showToast();
      } catch (e) {
        loginErrorToastRef.current?.showToast();
      }
    },
  });

  return (
    <Container>
      <FormContainer>
        <Toast ref={loginErrorToastRef} message={TOAST_ERROR} variant="error" />
        <Toast ref={loginSuccessToastRef} message="You're logged in" />
        <Title type={3}>Welcome back</Title>
        <TextField
          label="E-mail"
          name="email"
          type="email"
          value={values.email}
          error={touched.email && !!errors.email}
          errorText={errors.email}
          onChange={handleChange('email')}
        />
        <TextField
          label="Password"
          name="password"
          type="password"
          value={values.password}
          error={touched.password && !!errors.password}
          errorText={errors.password}
          onChange={handleChange('password')}
        />
      </FormContainer>
      <Button text="Log In" onClick={submitForm} />
    </Container>
  );
}
