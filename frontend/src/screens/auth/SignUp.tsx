import { Button, Container, TextField, Toast, ToastMethods } from 'components';
import { useFormik } from 'formik';
import * as yup from 'yup';
import { FormContainer, Title } from 'screens/common';
import { useAuthContext } from 'features/auth';
import { useRef } from 'react';
import { TOAST_ERROR } from 'utils/constants';

type Values = {
  name: string;
  surname: string;
  email: string;
  password: string;
  isAdmin: boolean;
};

const initialValues: Values = {
  name: '',
  surname: '',
  email: '',
  password: '',
  // TODO: change to false
  isAdmin: true,
};

export function SignUp() {
  const { signUp } = useAuthContext();

  const signupSuccessToastRef = useRef<ToastMethods>(null);
  const signupErrorToastRef = useRef<ToastMethods>(null);

  const validationSchema = yup.object().shape({
    name: yup
      .string()
      .min(3, 'First name must have minimum 3 chars')
      .required('First name is required'),
    surname: yup
      .string()
      .min(3, 'Last name must have minimum 3 chars')
      .required('Last name is required'),
    email: yup.string().required('E-mail is required').email('E-mail must be valid'),
    password: yup
      .string()
      .min(6, 'Password must have minimum 6 chars')
      .required('Password is required'),
  });

  const { values, errors, touched, handleChange, submitForm } = useFormik<Values>({
    initialValues,
    validationSchema,
    onSubmit: async () => {
      try {
        await signUp(values);
        signupSuccessToastRef.current?.showToast();
      } catch (e) {
        signupErrorToastRef.current?.showToast();
      }
    },
  });

  return (
    <Container>
      <FormContainer>
        <Toast ref={signupErrorToastRef} message={TOAST_ERROR} variant="error" />
        <Toast ref={signupSuccessToastRef} message="You're signed up" />
        <Title type={3}>I'm here for the first time</Title>
        <TextField
          label="First name"
          name="name"
          type="text"
          value={values.name}
          error={touched.name && !!errors.name}
          errorText={errors.name}
          onChange={handleChange('name')}
        />
        <TextField
          label="Last name"
          name="surname"
          type="text"
          value={values.surname}
          error={touched.surname && !!errors.surname}
          errorText={errors.surname}
          onChange={handleChange('surname')}
        />
        <TextField
          label="E-mail"
          name="email"
          type="email"
          value={values.email}
          error={touched.email && !!errors.email}
          errorText={errors.email}
          onChange={handleChange('email')}
        />
        <TextField
          label="Password"
          name="password"
          type="password"
          value={values.password}
          error={touched.password && !!errors.password}
          errorText={errors.password}
          onChange={handleChange('password')}
        />
      </FormContainer>
      <Button text="Sign Up" onClick={submitForm} />
    </Container>
  );
}
