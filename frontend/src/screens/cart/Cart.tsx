import { Button, Container, DishCard, Text, Toast, ToastMethods } from 'components';
import { useCartContext } from 'features/cart';
import { useRef } from 'react';
import { Title } from 'screens/common';
import styled from 'styled-components';
import { TOAST_ERROR } from 'utils/constants';

export function Cart() {
  const { cartDishes, orderCart } = useCartContext();

  const orderCartSuccessToastRef = useRef<ToastMethods>(null);
  const orderCartErrorToastRef = useRef<ToastMethods>(null);

  const handleOrder = async () => {
    try {
      await orderCart();
      orderCartSuccessToastRef.current?.showToast();
    } catch (e) {
      orderCartErrorToastRef.current?.showToast();
    }
  };

  return (
    <Container>
      <Toast ref={orderCartErrorToastRef} message={TOAST_ERROR} variant="error" />
      <Toast ref={orderCartSuccessToastRef} message="Cart succesfully ordered" />
      <Top>
        <Title type={3}>Cart</Title>
        <div>
          <Button text="Order cart" size="small" onClick={handleOrder} />
        </div>
      </Top>
      {!cartDishes.length && <Text.Subtitle type={2}>Your cart is empty </Text.Subtitle>}
      {cartDishes.map((dish) => (
        <DishCard key={dish.name} dish={dish} />
      ))}
    </Container>
  );
}
const Top = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`;
