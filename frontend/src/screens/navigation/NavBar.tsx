import { colors, Icon } from 'assets';
import { Text } from 'components';
import { Link, useLocation } from 'react-router-dom';
import { css, styled } from 'styled-components';
import { useEffect, useState } from 'react';
import { useAuthContext } from 'features/auth';
import { useUserContext } from 'features/user';

const TABS = {
  0: 'home',
  1: 'cart',
  2: 'login',
  3: 'signup',
  4: 'addDish',
};

function getColor(tab: string, selectedTab: string) {
  return tab === selectedTab ? colors.blueberry.blueberry550 : colors.base.black;
}

export function NavBar() {
  const location = useLocation();
  const { authUser, logOut } = useAuthContext();
  const { user } = useUserContext();

  const [selectedTab, setSelectedTab] = useState<string>('');

  useEffect(() => {
    setSelectedTab(location.pathname.split('/')[1]);
  }, [location.pathname]);

  return (
    <Container>
      <div>
        <Logo to="/">
          <Icon.Cutlery width={32} height={32} color={colors.accent.pink} />
          <Text.Title type={2} color={colors.accent.pink}>
            EatNow
          </Text.Title>
        </Logo>
      </div>
      <RightContainer>
        {authUser && (
          <NavLink to={TABS[1]} selected={selectedTab === TABS[1]}>
            <Icon.Cart width={18} height={18} color={getColor(TABS[1], selectedTab)} />
            <Text.Subtitle color={getColor(TABS[1], selectedTab)} type={3}>
              Cart
            </Text.Subtitle>
          </NavLink>
        )}

        {!authUser && (
          <NavLink to={TABS[2]} selected={selectedTab === TABS[2]}>
            <Icon.Person width={18} height={18} color={getColor(TABS[2], selectedTab)} />
            <Text.Subtitle color={getColor(TABS[2], selectedTab)} type={3}>
              Sign In
            </Text.Subtitle>
          </NavLink>
        )}
        {!authUser && (
          <NavLink to={TABS[3]} selected={selectedTab === TABS[3]}>
            <Icon.PersonAdd width={18} height={18} color={getColor(TABS[3], selectedTab)} />
            <Text.Subtitle color={getColor(TABS[3], selectedTab)} type={3}>
              Sign Up
            </Text.Subtitle>
          </NavLink>
        )}

        {user && user.isAdmin && (
          <NavLink to={TABS[4]} selected={selectedTab === TABS[4]}>
            <Icon.PlusCircle width={18} height={18} color={getColor(TABS[4], selectedTab)} />
            <Text.Subtitle color={getColor(TABS[3], selectedTab)} type={3}>
              Add dish
            </Text.Subtitle>
          </NavLink>
        )}
        {authUser && (
          <LogOutButton onClick={logOut}>
            <Icon.Person width={18} height={18} color={getColor(TABS[3], selectedTab)} />
            <Text.Subtitle color={getColor(TABS[3], selectedTab)} type={3}>
              Log Out
            </Text.Subtitle>
          </LogOutButton>
        )}
      </RightContainer>
    </Container>
  );
}

const Container = styled.div`
  display: flex;
  padding: 24px 32px;
  box-shadow: 0 2px 4px ${colors.gray.gray250};
  justify-content: space-between;
  align-items: center;
`;

const RightContainer = styled.div`
  display: flex;
  flex-direction: row;
  column-gap: 8px;
  align-items: center;
`;

const linkStyle = css`
  text-decoration: none;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
`;

const Logo = styled(Link)`
  ${linkStyle};
  column-gap: 8px;
`;

const NavLink = styled(Link)<{ selected: boolean }>`
  ${linkStyle};
  padding: 4px 32px;
  column-gap: 4px;

  ${(p) => {
    if (p.selected) {
      return css`
        border: 2px solid ${colors.blueberry.blueberry450};
        border-radius: 24px;
      `;
    }
  }}
`;

const LogOutButton = styled.button`
  ${linkStyle};
  padding: 4px 32px;
  column-gap: 4px;
  border: none;
  background-color: ${colors.base.white};

  &:hover {
    cursor: pointer;
  }
`;
