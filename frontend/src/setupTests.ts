// jest-dom adds custom jest matchers for asserting on DOM nodes.
// allows you to do things like:
// expect(element).toHaveTextContent(/react/i)
// learn more: https://github.com/testing-library/jest-dom
import '@testing-library/jest-dom';

jest.mock('./config/URLS.ts', () => ({
  get LOGIN_URL() {
    return 'http://localhost:8080/api/auth/login';
  },
  get SIGNUP_URL() {
    return 'http://localhost:8080/api/auth/signup';
  },
  get USER_URL() {
    return 'http://localhost:8080/api/users';
  },
  get DISHES_URL() {
    return 'http://localhost:8080/api/dishes';
  },
  get BASE_CART_URL() {
    return 'http://localhost:8080/api/carts/user';
  },
  get PENDING_CART_URL() {
    return (userId: number) => this.BASE_CART_URL + `/${userId}/pending`;
  },
  get UPDATE_CART_URL() {
    return (userId: number) => this.BASE_CART_URL + `/${userId}/updateQuantity`;
  },
  get ORDER_CART_URL() {
    return (userId: number) => `http://localhost:8080/api/carts/user/${userId}/orderCart`;
  },
}));
