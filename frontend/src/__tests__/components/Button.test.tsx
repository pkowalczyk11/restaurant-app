import { fireEvent, render, screen } from '@testing-library/react';
import { Button } from 'components/Button';

describe('Button', () => {
  it('renders Button and calls onClick', () => {
    const onClick = jest.fn();
    render(<Button text="Click me!" onClick={onClick} />);

    const button = screen.getByText('Click me!');
    fireEvent.click(button);
    expect(onClick).toBeCalled();
  });

  it('renders small Button with 180px width', () => {
    const onClick = jest.fn();
    render(<Button text="Click me!" onClick={onClick} size="small" />);

    const button = screen.getByText('Click me!');
    expect(button).toHaveStyle({ width: 180 });
  });
});
