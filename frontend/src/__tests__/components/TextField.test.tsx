import { render, screen } from '@testing-library/react';
import { TextField } from 'components/TextField';

describe('TextField', () => {
  it('renders TextField without crashing', () => {
    render(<TextField />);

    const input = screen.getByRole('textbox');
    expect(input).toBeInTheDocument();
  });

  it('renders TextField with error', () => {
    const errorText = 'Error text';
    render(<TextField error={true} errorText={errorText} />);

    const inputErrorText = screen.getByText(errorText);
    expect(inputErrorText).toBeInTheDocument();
  });
});
