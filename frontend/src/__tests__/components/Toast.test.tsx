import { fireEvent, render, screen, waitFor, act } from '@testing-library/react';
import { Toast, ToastMethods } from 'components/Toast';
import { createRef } from 'react';

describe('Toast', () => {
  it('shows Toast when showToast is called', async () => {
    const ref = createRef<ToastMethods>();
    const message = 'Toast message';

    render(<Toast ref={ref} message={message} />);

    act(() => {
      ref.current?.showToast();
    });

    await waitFor(() => {
      const toast = screen.getByText(message);
      expect(toast).toBeInTheDocument();
    });
  });

  it('hide Toast when hideToast is called', async () => {
    const ref = createRef<ToastMethods>();
    const message = 'Toast message';

    render(<Toast ref={ref} message={message} />);

    act(() => {
      ref.current?.showToast();
      ref.current?.hideToast();
    });

    await waitFor(() => {
      const toast = screen.queryByText(message);
      expect(toast).toBeNull();
    });
  });

  it('hide Toast when close button is clicked', async () => {
    const ref = createRef<ToastMethods>();
    const message = 'Toast message';

    render(<Toast ref={ref} message={message} />);

    act(() => {
      ref.current?.showToast();
    });

    const closeButton = screen.getByRole('button');
    fireEvent.click(closeButton);

    await waitFor(() => {
      const toast = screen.queryByText(message);
      expect(toast).toBeNull();
    });
  });
});
