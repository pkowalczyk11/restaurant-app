import { LogIn } from 'screens/auth/LogIn';
import { fireEvent, render, screen, waitFor } from '@testing-library/react';
import { useAuthContext } from 'features/auth/AuthContext';
import { TOAST_ERROR } from 'utils/constants';

jest.mock('features/auth/AuthContext');

describe('LogIn screen', () => {
  const mockLogin = jest.fn();

  beforeEach(() => {
    mockLogin.mockClear();
    (useAuthContext as jest.Mock).mockReturnValue({
      logIn: mockLogin,
    });
  });

  it('renders LogIn screen with initial state', async () => {
    render(<LogIn />);

    expect(screen.getByLabelText('E-mail')).toBeInTheDocument();
    expect(screen.getByLabelText('Password')).toBeInTheDocument();
    expect(screen.getByText('Log In')).toBeInTheDocument();
  });

  it('shows LogIn validation errors when fields are touched and left blank', async () => {
    render(<LogIn />);

    const emailInput = screen.getByLabelText('E-mail');
    const passwordInput = screen.getByLabelText('Password');
    const loginButton = screen.getByText('Log In');

    fireEvent.blur(emailInput);
    fireEvent.blur(passwordInput);
    fireEvent.click(loginButton);

    await waitFor(() => {
      expect(screen.getByText('E-mail is required')).toBeInTheDocument();
      expect(screen.getByText('Password is required')).toBeInTheDocument();
    });
  });

  it('shows LogIn error toast when onSubmit fails', async () => {
    mockLogin.mockImplementation(() => Promise.reject());
    render(<LogIn />);

    const emailTextField = screen.getByLabelText('E-mail');
    const passwordTextField = screen.getByLabelText('Password');
    const button = screen.getByText('Log In');

    fireEvent.change(emailTextField, { target: { value: 'test@example.com' } });
    fireEvent.change(passwordTextField, { target: { value: 'testPassword' } });
    fireEvent.click(button);

    await waitFor(() => {
      expect(screen.getByText(TOAST_ERROR)).toBeInTheDocument();
    });
  });

  it('shows LogIn success toast when onSubmit succeeds', async () => {
    // (useAuthContext as jest.Mock).mockReturnValue({
    //   logIn: mockLogin,
    // });
    mockLogin.mockResolvedValue(undefined);
    render(<LogIn />);

    const emailTextField = screen.getByLabelText('E-mail');
    const passwordTextField = screen.getByLabelText('Password');
    const button = screen.getByText('Log In');

    fireEvent.change(emailTextField, { target: { value: 'test@example.com' } });
    fireEvent.change(passwordTextField, { target: { value: 'testPassword' } });
    fireEvent.click(button);

    await waitFor(() => {
      expect(screen.getByText("You're logged in")).toBeInTheDocument();
    });
  });
});
