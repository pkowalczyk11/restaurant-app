import { fireEvent, render, screen, waitFor } from '@testing-library/react';
import { useCartContext } from 'features/cart/CartContext';
import { Cart } from 'screens/cart/Cart';
import { TOAST_ERROR } from 'utils/constants';

jest.mock('features/cart/CartContext');

describe('Cart screen', () => {
  const mockOrderCart = jest.fn();

  beforeEach(() => {
    mockOrderCart.mockClear();

    (useCartContext as jest.Mock).mockReturnValue({
      cartDishes: [{ id: 1, name: 'Test Dish', price: 10, quantity: 2 }],
      updateCart: jest.fn(),
      orderCart: mockOrderCart,
      getDishQuantity: jest.fn().mockReturnValue(1),
    });
  });

  it('renders Cart screen correctly and checks cart contents', () => {
    render(<Cart />);

    expect(screen.getByText('Test Dish')).toBeInTheDocument();
    expect(screen.getByText('Order cart')).toBeEnabled();
  });

  it('shows error toast when orderCart fails', async () => {
    mockOrderCart.mockImplementation(() => Promise.reject());
    render(<Cart />);

    const button = screen.getByText('Order cart');
    fireEvent.click(button);

    await waitFor(() => {
      expect(screen.getByText(TOAST_ERROR)).toBeInTheDocument();
    });
  });
});
