package pl.pk.edu.restaurant.api.controller

import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.media.ArraySchema
import io.swagger.v3.oas.annotations.media.Content
import io.swagger.v3.oas.annotations.media.Schema
import io.swagger.v3.oas.annotations.responses.ApiResponse
import io.swagger.v3.oas.annotations.responses.ApiResponses
import io.swagger.v3.oas.annotations.security.SecurityRequirement
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import pl.pk.edu.restaurant.api.ErrorResponse
import pl.pk.edu.restaurant.api.model.DishInfo
import pl.pk.edu.restaurant.api.model.request.DishRequest
import pl.pk.edu.restaurant.auth.AuthenticatedUserService
import pl.pk.edu.restaurant.database.service.DishService

@RestController
@RequestMapping("api/dishes", produces = [MediaType.APPLICATION_JSON_VALUE, MediaType.TEXT_PLAIN_VALUE])
@CrossOrigin
class DishController(private val dishService: DishService,
                     private val authenticatedUserService: AuthenticatedUserService
) {

    @Operation(summary = "Get all dishes")
    @ApiResponses(
        value = [
            ApiResponse(responseCode = "200", description = "Dishes list returned",
                content = [Content(mediaType = "application/json",
                    array = ArraySchema(schema = Schema(implementation = DishInfo::class)))]),
            ApiResponse(responseCode = "500", description = "Internal server error",
                content = [Content(mediaType = "application/json",
                    schema = Schema(implementation = ErrorResponse::class))])
        ])
    @GetMapping("")
    fun getDishes(): ResponseEntity<List<DishInfo>> {
        return ResponseEntity.ok(dishService.getAllDishes().map { DishInfo.fromDbDish(it) })
    }

    @Operation(summary = "Add dish")
    @ApiResponses(
        value = [
            ApiResponse(responseCode = "201", description = "Dish has been created",
                content = [Content(mediaType = "application/json",
                    schema = Schema(implementation = DishInfo::class))]),
            ApiResponse(responseCode = "403", description = "Dish name is not available",
                content = [Content(mediaType = "application/json",
                    schema = Schema(implementation = ErrorResponse::class))]),
            ApiResponse(responseCode = "404", description = "Dish not found",
                content = [Content(mediaType = "application/json",
                    schema = Schema(implementation = ErrorResponse::class))]),
            ApiResponse(responseCode = "401", description = "Unauthorized",
                content = [Content(mediaType = "application/json",
                    schema = Schema(implementation = ErrorResponse::class))]),
            ApiResponse(responseCode = "500", description = "Internal server error",
                content = [Content(mediaType = "application/json",
                    schema = Schema(implementation = ErrorResponse::class))])
        ])
    @SecurityRequirement(name = "Admin Authentication")
    @PostMapping("", consumes = [MediaType.APPLICATION_JSON_VALUE])
    @PreAuthorize("@authenticatedUserService.getAuthenticated().isAdmin")
    fun addDish(@RequestBody dishRequest: DishRequest): ResponseEntity<Any> {
        if (!dishService.isDishNameAvailable(dishRequest.name)) {
            return ResponseEntity
                .status(HttpStatus.FORBIDDEN)
                .body(ErrorResponse("Dish name not available!"))
        }
        val dish = dishService.addDish(dishRequest.toDbDish())
        return ResponseEntity
            .status(HttpStatus.CREATED)
            .body(DishInfo.fromDbDish(dish))
    }

    @Operation(summary = "Delete dish")
    @ApiResponses(
        value = [
            ApiResponse(responseCode = "200", description = "Dish has been removed",
                content = [Content(mediaType = "text/plain")]),
            ApiResponse(responseCode = "404", description = "Dish not found",
                content = [Content(mediaType = "application/json",
                    schema = Schema(implementation = ErrorResponse::class))]),
            ApiResponse(responseCode = "401", description = "Unauthorized",
                content = [Content(mediaType = "application/json",
                    schema = Schema(implementation = ErrorResponse::class))]),
            ApiResponse(responseCode = "500", description = "Internal server error",
                content = [Content(mediaType = "application/json",
                    schema = Schema(implementation = ErrorResponse::class))])
        ])
    @SecurityRequirement(name = "Admin Authentication")
    @DeleteMapping("/{dishId}")
    @PreAuthorize("@authenticatedUserService.getAuthenticated().isAdmin")
    fun deleteDish(@PathVariable dishId: Int): ResponseEntity<String> {
        dishService.deleteDishById(dishId)
        return ResponseEntity.ok("Dish deleted successfully")
    }
}