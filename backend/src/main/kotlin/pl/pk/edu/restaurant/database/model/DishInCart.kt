package pl.pk.edu.restaurant.database.model

import jakarta.persistence.Column
import jakarta.persistence.Entity
import jakarta.persistence.GeneratedValue
import jakarta.persistence.GenerationType
import jakarta.persistence.Id
import jakarta.persistence.JoinColumn
import jakarta.persistence.ManyToOne
import jakarta.persistence.Table

@Entity
@Table(name = "dish_in_cart")
class DishInCart(
    @ManyToOne(cascade = [])
    @JoinColumn(name = "dish_id", referencedColumnName = "id")
    val dish: Dish,

    @ManyToOne(cascade = [])
    @JoinColumn(name = "cart_id", referencedColumnName = "id")
    val cart: Cart,

    @Column(name = "price")
    val price: Double,

    @Column(name = "quantity")
    var quantity: Int,

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    val id: Int? = null
) {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as DishInCart

        if (dish != other.dish) return false
        if (price != other.price) return false
        if (quantity != other.quantity) return false

        return true
    }

    override fun hashCode(): Int {
        var result = dish.hashCode()
        result = 31 * result + price.hashCode()
        result = 31 * result + quantity
        return result
    }
}