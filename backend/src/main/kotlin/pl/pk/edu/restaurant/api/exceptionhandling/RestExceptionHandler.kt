package pl.pk.edu.restaurant.api.exceptionhandling

import io.swagger.v3.oas.annotations.Hidden
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.HttpStatusCode
import org.springframework.http.ResponseEntity
import org.springframework.http.converter.HttpMessageNotReadableException
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestControllerAdvice
import org.springframework.web.context.request.WebRequest
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler
import pl.pk.edu.restaurant.api.ErrorResponse
import pl.pk.edu.restaurant.exceptions.NotFoundException
import pl.pk.edu.restaurant.exceptions.UnauthorizedException

@RestControllerAdvice
class RestExceptionHandler : ResponseEntityExceptionHandler() {

    @Override
    override fun handleHttpMessageNotReadable(ex: HttpMessageNotReadableException, headers: HttpHeaders,
                                              status: HttpStatusCode, request: WebRequest): ResponseEntity<Any> =
        ResponseEntity
            .status(HttpStatus.BAD_REQUEST)
            .body(ErrorResponse("Request parsing failed!"))

    @ExceptionHandler(NotFoundException::class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @Hidden
    fun handleNotFoundException(ex: RuntimeException): ResponseEntity<ErrorResponse> =
        ResponseEntity(ErrorResponse(ex.message!!), HttpStatus.NOT_FOUND)

    @ExceptionHandler(UnauthorizedException::class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    @Hidden
    fun handleUnauthorizedException(ex: RuntimeException): ResponseEntity<ErrorResponse> =
        ResponseEntity(ErrorResponse(ex.message!!), HttpStatus.UNAUTHORIZED)

    @ExceptionHandler(Exception::class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @Hidden
    fun handleException(ex: Exception): ResponseEntity<ErrorResponse> =
        ResponseEntity(ErrorResponse(ex.message!!), HttpStatus.INTERNAL_SERVER_ERROR)
}