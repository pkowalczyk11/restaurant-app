package pl.pk.edu.restaurant.database.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import pl.pk.edu.restaurant.database.model.Cart
import pl.pk.edu.restaurant.database.model.CartState
import pl.pk.edu.restaurant.database.model.User

@Repository
interface CartRepository: JpaRepository<Cart, Int> {

    fun findByUserAndState(user: User, state: CartState): Cart?

    fun findAllByUser(user: User): List<Cart>
}