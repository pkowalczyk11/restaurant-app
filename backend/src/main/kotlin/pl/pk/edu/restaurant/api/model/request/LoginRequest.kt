package pl.pk.edu.restaurant.api.model.request

data class LoginRequest(val email: String,
                        val password: String)
