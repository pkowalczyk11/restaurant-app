package pl.pk.edu.restaurant.api.model

import pl.pk.edu.restaurant.database.model.User

data class UserInfo(val name: String,
                    val surname: String,
                    val email: String,
                    val isAdmin: Boolean,
                    val id: Int) {

    companion object {

        fun fromDbUser(user: User): UserInfo = UserInfo(user.name, user.surname, user.email, user.isAdmin, user.id!!)
    }
}
