package pl.pk.edu.restaurant.api.controller

import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.media.ArraySchema
import io.swagger.v3.oas.annotations.media.Content
import io.swagger.v3.oas.annotations.media.Schema
import io.swagger.v3.oas.annotations.responses.ApiResponse
import io.swagger.v3.oas.annotations.responses.ApiResponses
import io.swagger.v3.oas.annotations.security.SecurityRequirement
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import pl.pk.edu.restaurant.api.ErrorResponse
import pl.pk.edu.restaurant.api.model.CartInfo
import pl.pk.edu.restaurant.api.model.request.CartDishRequest
import pl.pk.edu.restaurant.auth.AuthenticatedUserService
import pl.pk.edu.restaurant.database.service.CartService
import pl.pk.edu.restaurant.database.service.UserService

@RestController
@RequestMapping("/api/carts", produces = [MediaType.APPLICATION_JSON_VALUE])
@CrossOrigin
class CartController(private val cartService: CartService,
                     private val userService: UserService,
                     private val authenticatedUserService: AuthenticatedUserService) {

    @Operation(summary = "Get user's pending cart")
    @ApiResponses(
        value = [
            ApiResponse(responseCode = "200", description = "User's pending cart returned",
                content = [Content(mediaType = "application/json",
                    schema = Schema(implementation = CartInfo::class))]),
            ApiResponse(responseCode = "404", description = "Cart not found",
                content = [Content(mediaType = "application/json",
                    schema = Schema(implementation = ErrorResponse::class))]),
            ApiResponse(responseCode = "401", description = "Unauthorized",
                content = [Content(mediaType = "application/json",
                    schema = Schema(implementation = ErrorResponse::class))]),
            ApiResponse(responseCode = "500", description = "Internal server error",
                content = [Content(mediaType = "application/json",
                    schema = Schema(implementation = ErrorResponse::class))])
        ])
    @SecurityRequirement(name = "Bearer Authentication")
    @GetMapping("/user/{userId}/pending")
    @PreAuthorize("@authenticatedUserService.getAuthenticatedId() == #userId")
    fun getPendingCart(@PathVariable userId: Int): ResponseEntity<CartInfo> {
        val user = userService.getById(userId)
        return ResponseEntity.ok(CartInfo.fromDbCart(cartService.findPendingCartByUser(user)))
    }

    @Operation(summary = "Update dish quantity in cart")
    @ApiResponses(
        value = [
            ApiResponse(responseCode = "201", description = "Dish quantity updated",
                content = [Content(mediaType = "application/json",
                    schema = Schema(implementation = CartInfo::class))]),
            ApiResponse(responseCode = "404", description = "Dish not found",
                content = [Content(mediaType = "application/json",
                    schema = Schema(implementation = ErrorResponse::class))]),
            ApiResponse(responseCode = "401", description = "Unauthorized",
                content = [Content(mediaType = "application/json",
                    schema = Schema(implementation = ErrorResponse::class))]),
            ApiResponse(responseCode = "500", description = "Internal server error",
                content = [Content(mediaType = "application/json",
                    schema = Schema(implementation = ErrorResponse::class))])
        ])
    @SecurityRequirement(name = "Bearer Authentication")
    @PostMapping("/user/{userId}/updateQuantity", consumes = [MediaType.APPLICATION_JSON_VALUE])
    @PreAuthorize("@authenticatedUserService.getAuthenticatedId() == #userId")
    fun updateDishQuantity(@PathVariable userId: Int,
                           @RequestBody cartDishRequest: CartDishRequest): ResponseEntity<CartInfo> {
        val user = userService.getById(userId)
        return ResponseEntity
            .status(HttpStatus.CREATED)
            .body(CartInfo.fromDbCart(cartService.updateDishQuantity(cartDishRequest.dishId,
                cartDishRequest.quantity, user)))
    }

    @Operation(summary = "Change cart state to ordered")
    @ApiResponses(
        value = [
            ApiResponse(responseCode = "200", description = "Cart's state changed",
                content = [Content(mediaType = "application/json",
                    schema = Schema(implementation = CartInfo::class))]),
            ApiResponse(responseCode = "404", description = "Cart not found",
                content = [Content(mediaType = "application/json",
                    schema = Schema(implementation = ErrorResponse::class))]),
            ApiResponse(responseCode = "401", description = "Unauthorized",
                content = [Content(mediaType = "application/json",
                    schema = Schema(implementation = ErrorResponse::class))]),
            ApiResponse(responseCode = "500", description = "Internal server error",
                content = [Content(mediaType = "application/json",
                    schema = Schema(implementation = ErrorResponse::class))])
        ])
    @SecurityRequirement(name = "Bearer Authentication")
    @PutMapping("/{cartId}/orderCart")
    fun orderCart(@PathVariable cartId: Int): ResponseEntity<CartInfo> =
        ResponseEntity.ok(CartInfo.fromDbCart(cartService.orderCart(cartId,
            authenticatedUserService.getAuthenticatedId())))

    @Operation(summary = "Get all user's carts")
    @ApiResponses(
        value = [
            ApiResponse(responseCode = "200", description = "User's carts returned",
                content = [Content(mediaType = "application/json",
                    array = ArraySchema(schema = Schema(implementation = CartInfo::class)))]),
            ApiResponse(responseCode = "401", description = "Unauthorized",
                content = [Content(mediaType = "application/json",
                    schema = Schema(implementation = ErrorResponse::class))]),
            ApiResponse(responseCode = "500", description = "Internal server error",
                content = [Content(mediaType = "application/json",
                    schema = Schema(implementation = ErrorResponse::class))])
        ])
    @SecurityRequirement(name = "Bearer Authentication")
    @GetMapping("/user/{userId}")
    @PreAuthorize("@authenticatedUserService.getAuthenticated().isAdmin " +
        "or @authenticatedUserService.getAuthenticatedId() == #userId")
    fun getUsersCarts(@PathVariable userId: Int): ResponseEntity<List<CartInfo>> {
        val carts = cartService.findAllUsersCarts(userService.getById(userId))
        return ResponseEntity.ok(carts.map { CartInfo.fromDbCart(it) })
    }
}