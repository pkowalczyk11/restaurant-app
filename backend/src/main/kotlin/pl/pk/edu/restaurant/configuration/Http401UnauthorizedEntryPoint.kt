package pl.pk.edu.restaurant.configuration

import jakarta.servlet.http.HttpServletRequest
import jakarta.servlet.http.HttpServletResponse
import org.springframework.http.HttpStatus
import org.springframework.security.core.AuthenticationException
import org.springframework.security.web.AuthenticationEntryPoint
import org.springframework.stereotype.Component

@Component
class Http401UnauthorizedEntryPoint: AuthenticationEntryPoint {

    override fun commence(request: HttpServletRequest, response: HttpServletResponse,
                          authException: AuthenticationException?) {
        response.sendError(HttpStatus.UNAUTHORIZED.value(), "Access denied")
    }
}