package pl.pk.edu.restaurant.database.service

import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import pl.pk.edu.restaurant.database.model.Cart
import pl.pk.edu.restaurant.database.model.CartState
import pl.pk.edu.restaurant.database.model.DishInCart
import pl.pk.edu.restaurant.database.model.User
import pl.pk.edu.restaurant.database.repository.CartRepository
import pl.pk.edu.restaurant.database.repository.DishRepository
import pl.pk.edu.restaurant.exceptions.NotFoundException
import pl.pk.edu.restaurant.exceptions.UnauthorizedException

@Service
class CartService(private val cartRepository: CartRepository,
                  private val dishRepository: DishRepository) {

    @Transactional
    fun updateDishQuantity(dishId: Int, quantity: Int, user: User): Cart {
        val cart = cartRepository.findByUserAndState(user, CartState.PENDING) ?: Cart(user)
        val dishInCart = cart.dishes.firstOrNull { it.dish.id == dishId } ?: createDishInCart(dishId, quantity, cart)
        dishInCart.quantity = quantity
        if (cart.dishes.none { it.dish.id == dishId }) {
            cart.dishes.addLast(dishInCart)
        }
        return cartRepository.save(cart)
    }

    @Transactional
    fun orderCart(cartId: Int, userId: Int?): Cart {
        val cart = cartRepository.findByIdOrNull(cartId) ?: throw NotFoundException("Cart with $cartId id not found")
        if (cart.user.id != userId) {
            throw UnauthorizedException("Cannot order cart that doesn't belong to a user")
        }
        cart.state = CartState.ORDERED
        return cartRepository.save(cart)
    }

    fun findPendingCartByUser(user: User): Cart = cartRepository.findByUserAndState(user, CartState.PENDING)
        ?: throw NotFoundException("No pending cart for given user")

    fun findAllUsersCarts(user: User): List<Cart> = cartRepository.findAllByUser(user)

    private fun createDishInCart(dishId: Int, quantity: Int, cart: Cart): DishInCart {
        val dish = dishRepository.findByIdOrNull(dishId) ?: throw NotFoundException("Dish with $dishId id not found")
        return DishInCart(dish, cart, dish.price, quantity)
    }
}