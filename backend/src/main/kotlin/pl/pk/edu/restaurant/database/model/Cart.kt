package pl.pk.edu.restaurant.database.model

import jakarta.persistence.CascadeType
import jakarta.persistence.Column
import jakarta.persistence.Entity
import jakarta.persistence.EnumType
import jakarta.persistence.Enumerated
import jakarta.persistence.GeneratedValue
import jakarta.persistence.GenerationType
import jakarta.persistence.Id
import jakarta.persistence.JoinColumn
import jakarta.persistence.ManyToOne
import jakarta.persistence.OneToMany
import jakarta.persistence.Table

@Entity
@Table(name = "cart")
class Cart(
    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "id", nullable = false)
    val user: User,

    @Column(name = "state")
    @Enumerated(EnumType.STRING)
    var state: CartState = CartState.PENDING,

    @OneToMany(cascade = [CascadeType.ALL])
    val dishes: MutableList<DishInCart> = mutableListOf(),

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    val id: Int? = null
) {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Cart

        if (user != other.user) return false
        if (state != other.state) return false
        if (dishes != other.dishes) return false

        return true
    }

    override fun hashCode(): Int {
        var result = user.hashCode()
        result = 31 * result + state.hashCode()
        result = 31 * result + dishes.hashCode()
        return result
    }
}