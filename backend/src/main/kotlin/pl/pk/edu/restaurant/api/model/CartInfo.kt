package pl.pk.edu.restaurant.api.model

import pl.pk.edu.restaurant.database.model.Cart
import pl.pk.edu.restaurant.database.model.CartState

data class CartInfo(val dishes: List<CartDishInfo>,
                    val cartId: Int,
                    val state: CartState) {

    companion object {

        fun fromDbCart(cart: Cart) =
            CartInfo(cart.dishes.map { CartDishInfo(it.dish.name, it.price, it.quantity, it.id!!) }, cart.id!!,
                cart.state)
    }

    data class CartDishInfo(val dishName: String,
                            val price: Double,
                            val quantity: Int,
                            val id: Int)
}
