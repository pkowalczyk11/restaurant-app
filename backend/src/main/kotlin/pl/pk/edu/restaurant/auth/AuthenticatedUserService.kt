package pl.pk.edu.restaurant.auth

import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Service
import pl.pk.edu.restaurant.database.model.User
import pl.pk.edu.restaurant.database.repository.UserRepository

@Service
class AuthenticatedUserService(private val userRepository: UserRepository) {

    fun getAuthenticatedId(): Int? {
        val email = (SecurityContextHolder.getContext().authentication.principal as User).email
        return userRepository.findByEmail(email)?.id
    }

    fun getAuthenticated(): User {
        val email = (SecurityContextHolder.getContext().authentication.principal as User).email
        return userRepository.findByEmail(email)!!
    }
}
