package pl.pk.edu.restaurant.configuration

import io.swagger.v3.oas.annotations.enums.SecuritySchemeType
import io.swagger.v3.oas.annotations.security.SecurityScheme
import org.springframework.context.annotation.Configuration

@Configuration
@SecurityScheme(
    name = "Bearer Authentication",
    description = "A JWT token is required to access this endpoint. Provide correct JWT token obtained from " +
        "/api/auth endpoints",
    type = SecuritySchemeType.HTTP,
    bearerFormat = "JWT",
    scheme = "bearer"
)
@SecurityScheme(
    name = "Admin Authentication",
    description = "A JWT token with admin privileges is required to access this endpoint. Provide correct " +
        "JWT token obtained from /api/auth endpoints",
    type = SecuritySchemeType.HTTP,
    bearerFormat = "JWT",
    scheme = "bearer"
)
class OpenAPIConfiguration