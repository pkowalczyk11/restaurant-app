package pl.pk.edu.restaurant.api.model.request

import pl.pk.edu.restaurant.database.model.User

data class SignUpRequest(val email: String,
                         val password: String,
                         val name: String,
                         val surname: String,
                         val isAdmin: Boolean = false) {

    fun toDbUser(): User = User(name, surname, email, password, isAdmin)
}
