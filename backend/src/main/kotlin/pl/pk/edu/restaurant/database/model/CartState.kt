package pl.pk.edu.restaurant.database.model

enum class CartState {
    PENDING,
    ORDERED
}