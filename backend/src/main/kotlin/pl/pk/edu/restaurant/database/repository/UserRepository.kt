package pl.pk.edu.restaurant.database.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import pl.pk.edu.restaurant.database.model.User

@Repository
interface UserRepository : JpaRepository<User, Int> {

    fun findByEmail(email: String): User?

    fun existsByEmail(email: String): Boolean
}