package pl.pk.edu.restaurant.database.service

import org.springframework.data.repository.findByIdOrNull
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import pl.pk.edu.restaurant.database.model.User
import pl.pk.edu.restaurant.database.repository.UserRepository
import pl.pk.edu.restaurant.exceptions.NotFoundException

@Service
class UserService(private val userRepository: UserRepository,
                  private val passwordEncoder: PasswordEncoder) : UserDetailsService {

    override fun loadUserByUsername(email: String): UserDetails =
        userRepository.findByEmail(email) ?: throw UsernameNotFoundException("No user with given email")

    fun getById(id: Int): User =
        userRepository.findByIdOrNull(id) ?: throw NotFoundException("No user with given id")

    @Transactional
    fun addUser(user: User): User {
        user.passwd = passwordEncoder.encode(user.passwd)
        return userRepository.save(user)
    }

    fun isEmailAvailable(email: String): Boolean = !userRepository.existsByEmail(email)
}