package pl.pk.edu.restaurant.api

data class ErrorResponse(val message: String)
