package pl.pk.edu.restaurant.api.controller

import io.mockk.every
import io.mockk.mockk
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import org.springframework.http.HttpStatus
import pl.pk.edu.restaurant.api.model.CartInfo
import pl.pk.edu.restaurant.api.model.request.CartDishRequest
import pl.pk.edu.restaurant.auth.AuthenticatedUserService
import pl.pk.edu.restaurant.database.model.*
import pl.pk.edu.restaurant.database.service.CartService
import pl.pk.edu.restaurant.database.service.UserService

class CartControllerTest {
    private val user = mockk<User>()
    private val cart = mockk<Cart>()
    private val dish = mockk<Dish>()

    private val authenticatedUserService = mockk<AuthenticatedUserService>()
    private val userService = mockk<UserService>()
    private val cartService = mockk<CartService>()
    private val cartController = CartController(cartService, userService, authenticatedUserService)

    @Test
    fun `should return 201 status code when dish quantity in cart updated`() {
        // given
        val quantity = 3
        val dishId = 1

        every { user.id } returns 1
        every { dish.id } returns dishId
        every { dish.name } returns "name"
        every { dish.price } returns 10.0

        every { userService.getById(any()) } returns user

        val cartDishRequest = CartDishRequest(dishId, quantity)

        every { cartService.updateDishQuantity(dishId, quantity, user) } returns cart
        every { cart.dishes } returns mutableListOf(DishInCart(dish, cart, dish.price, quantity, 1))
        every { cart.id } returns 1
        every { cart.state } returns CartState.PENDING

        // when
        val response = cartController.updateDishQuantity(user.id!!, cartDishRequest)

        // then
        assertThat(response.body).isInstanceOf(CartInfo::class.java)
        assertThat(response.statusCode).isEqualTo(HttpStatus.CREATED)

        val responseBody = response.body as CartInfo
        assertThat(responseBody.cartId).isEqualTo(cart.id)
        assertThat(responseBody.state).isEqualTo(cart.state)
        assertThat(responseBody.dishes).hasSize(cart.dishes.size)
        assertThat(responseBody.dishes[0].dishName).isEqualTo(dish.name)
    }

}
